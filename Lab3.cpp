/**
 * File name: Lab3
 * This program asks the user to supply 3 integers and then solves the quadratic equation and provides the results of the calculation.
 * 
 * Programmer: Sarah Ridolfi
 * Date created: 9/22/15
 */
#include <iostream>
#include <iomanip>
#include <cmath>
#include <conio.h>
using namespace std;

int main()
{
	int a, b, c = 0;
	double disc;

	cout << "This program will solve for x using the quadratic equation: ax^2 + bx + c = 0." << endl << endl;
	cout << "Please provide 3 integers. Note: Coefficient of x^2 must be non-zero." << endl << endl;
	cout << "Coefficient of x^2 (a) = ";
	cin >> a;

	while (a == 0) //if a = 0, this equation is not quadratic 
	{
		cout << endl << "Cannot be zero. Please re-enter." << endl;
		cout << "Coefficient of x^2 (a) = ";
		cin.clear();
		cin >> a;
	} 

	cout << "Coefficient of x (b) = ";
	cin >> b;
	cout << "Constant term (c) = ";
	cin >> c;

	//Calculate Solutions & Display Appropriate Output
	disc = (pow(b,2)) - (4*a*c); //Solve for the solution under the square root 
	cout.setf(ios::showpoint);
	cout.setf(ios::fixed);
	cout << setprecision(2);

	if (disc == 0)
	{
		double x = (-b) / (2 * a);
		cout << "There is one solution. x = " << x;
	}
	else if (disc < 0)
	{
		cout << "This solution has two complex roots.";
	}
	else
	{
		double x1 = (-b + sqrt(disc)) / (2 * a);
		double x2 = (-b - sqrt(disc)) / (2 * a);
		cout << "There are two distinct real solutions. x = " << x1 << " and " << x2;
	}

	cout.unsetf(ios::showpoint);
	cout << endl;
	_getch();
	return 0;
}
