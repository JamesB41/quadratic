#include <iostream>
#include <iomanip>
#include <cmath>
#include <vector>

using namespace std;

void setupStream();
void outputIntroText();
void getUserInput(int &, int &, int &);
void cleanupStream();
void getRoots(int, int, int, vector<int>&);

int main()
{
	int a, b, c = 0;
	vector<int> roots;

	setupStream();
	outputIntroText();
	getUserInput(a, b, c);
	getRoots(a, b, c, roots);

	cout << "Here are the roots: " << endl;

	for (int i = 0; i < roots.size(); i++)
	{
		cout << roots.at(i) << endl;
	}

	return 0;
}

void setupStream()
{
	cout.setf(ios::showpoint);
	cout.setf(ios::fixed);
	cout << setprecision(2);
}

void cleanupStream()
{
	cout.unsetf(ios::showpoint);
}

void outputIntroText()
{
	cout << "This program will solve for x using the quadratic equation: ax^2 + bx + c = 0." << endl << endl;
	cout << "Please provide 3 integers. Note: Coefficient of x^2 must be non-zero." << endl << endl;
}

void getUserInput(int &a, int &b, int &c)
{
	cout << "Coefficient of x^2 (a) = ";
	cin >> a;

	while (a == 0) //if a = 0, this equation is not quadratic 
	{
		cout << endl << "Cannot be zero. Please re-enter: ";
		cin >> a;
		cin.clear();
	} 

	cout << "Coefficient of x (b) = ";
	cin >> b;
	cout << "Constant term (c) = ";
	cin >> c;
	cout << endl;
}

void getRoots(int a, int b, int c, vector<int> &r)
{
	double disc;

	//Calculate Solutions & Display Appropriate Output
	disc = (pow(b,2)) - (4*a*c); //Solve for the solution under the square root 

	if (disc == 0)
	{
		double x = (-b) / (2 * a);
		r.push_back(x);
	}
	else if (disc < 0)
	{
		r.push_back(-1);
		r.push_back(-1);
	}
	else
	{
		double x1 = (-b + sqrt(disc)) / (2 * a);
		double x2 = (-b - sqrt(disc)) / (2 * a);
		r.push_back(x1);
		r.push_back(x2);
	}
}
